using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Pointilles : MonoBehaviour
{
    public GameObject point;
    public int NBPoints;
    public List<GameObject> listePoints = new List<GameObject>();
    public float DistancePoint;
    public AudioSource source;

    public Transform pivot;
    public GameObject stock;

    private void Start()
    {
        stock = new GameObject("Stockage Points");
        for (int i = 0; i < NBPoints; i++)
        {
            GameObject P = Instantiate(point, transform.position, Quaternion.identity, stock.transform);
            listePoints.Add(P);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(pivot.position, listePoints.Last().transform.position) > DistancePoint)
        {
            GameObject D = listePoints.First();
            PointAnim PA = D.GetComponent<PointAnim>();
            PA.Die();
            listePoints.RemoveAt(0);
            source.Play();

            foreach(GameObject G in listePoints)
            {
                G.GetComponent<PointAnim>().Shrink(NBPoints);
            }
            GameObject P = Instantiate(point, transform.position, Quaternion.identity, stock.transform);
            listePoints.Add(P);
        }
    }
}
