using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public Control playerPrefab;

    //public Camera mainCamera;

    public CinemachineVirtualCamera cinemachineCamera;

    public Camera mainCamera;
    
    [HideInInspector] public Control Player;

    // Start is called before the first frame update
    void Start()
    {
        Player = Instantiate(playerPrefab, this.transform.position, Quaternion.identity);
        //Player.transform.position = new Vector3(Player.transform.position.x, (Player.transform.position.y + 1 + ((SpriteRenderer)playerPrefab.GetComponentInChildren<SpriteRenderer>()).bounds.size.y / 2), 0);
        Player.transform.position = new Vector3(Player.transform.position.x, (Player.transform.position.y + 10));

        Player.mainCamera = mainCamera;

        Entities.player = Player;

        //mainCamera.transform.parent = Player.transform;

        //mainCamera.transform.localRotation = Quaternion.identity;
        //mainCamera.transform.localPosition = Vector3.zero;
        //mainCamera.transform.localScale = Vector3.one;
        if (cinemachineCamera == null)
            return;

        cinemachineCamera.Follow = Player.transform;
        cinemachineCamera.LookAt = Player.transform;
    }

    //// Update is called once per frame
    //void Update()
    //{
        
    //}
}
