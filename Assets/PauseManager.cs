using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    public Canvas pauseCanvas;
    public GameObject MainPanel;
    public GameObject OptionsPanel;
    public Slider AudioVolume;
    public Toggle RotationKeyboard;
    public Toggle RotationTheremin;
    public Toggle RotationEyetracker;
    public Toggle AccelerationKeyboard;
    public Toggle AccelerationTheremin;
    public Toggle AccelerationMouse;
    public bool isPaused = false;
    public Control player;

    void Start()
    {
        LoadValue();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseCanvas.gameObject.activeSelf)
                Resume();
            else
                SetPause();
        }
    }

    public void SetPause()
    {
        player.PausePlayer(true);
        OptionsPanel.SetActive(false);
        MainPanel.SetActive(true);
        pauseCanvas.gameObject.SetActive(true);
    }

    private void LoadValue()
    {
        switch (Options.rotation_control)
        {
            case (int)Options.Rotation_Control.KeyBoard:
                RotationKeyboard.isOn = true;
                break;
            case (int)Options.Rotation_Control.Theremin:
                RotationTheremin.isOn = true;
                break;
            case (int)Options.Rotation_Control.EyeTracking:
                RotationEyetracker.isOn = true;
                break;
        }

        switch (Options.deplacement_control)
        {
            case (int)Options.Acceleration_Control.KeyBoard:
                AccelerationKeyboard.isOn = true;
                break;
            case (int)Options.Acceleration_Control.Theremin:
                AccelerationTheremin.isOn = true;
                break;
            case (int)Options.Acceleration_Control.Mouse:
                AccelerationMouse.isOn = true;
                break;
        }

        AudioVolume.value = AudioListener.volume;
    }

    public void Resume()
    {
        player.InstantiateControl();
        pauseCanvas.gameObject.SetActive(false);
        player.PausePlayer(false); 
    }
}
