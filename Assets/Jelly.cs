using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Jelly : MonoBehaviour
{
    public float MaxVelocity;

    public bool BlockTheMovement = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider collision)
    {
        Control player = collision.gameObject.GetComponentInParent<Control>();

        if (player == false)
            return;

        Rigidbody rigidbody = player.GetComponent<Rigidbody>();

        rigidbody.velocity = Vector2.ClampMagnitude(rigidbody.velocity, MaxVelocity);


        player.isInJelly = true;
        //player.toggleThruster(false);
        if (BlockTheMovement)
        {
            player.canMove = false;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        Control player = collision.gameObject.GetComponentInParent<Control>();

        if (player == false)
            return;

        player.isInJelly = false;
        player.canMove = true;
    }
}
