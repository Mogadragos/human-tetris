using UnityEngine.Audio;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds2D;
    public Sound[] sounds3D;

    public string startSound;
    public string ambientSound;

    public static AudioManager instance;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else
        {
            instance.Stop2D(instance.ambientSound);

            instance.startSound = startSound;
            instance.ambientSound = ambientSound;
            instance.PlayStartSounds();

            Destroy(gameObject);

            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach(Sound s in sounds2D)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {
        PlayStartSounds();
    }

    void PlayStartSounds()
    {
        float delay = 0f;
        if (startSound != "")
            delay = Play2D(startSound);

        if (ambientSound != "")
            if (delay > 0.2f)
                Invoke(nameof(PlayAmbientSound), delay - 0.2f);
            else
                PlayAmbientSound();
    }

    void PlayAmbientSound()
    {
        Play2D(ambientSound);
    }

    Sound CheckSound(string name, Sound[] soundArray)
    {
        Sound s = Array.Find(soundArray, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return null;
        }
        return s;
    }

    public float Play2D (string name)
    {
        Sound s = CheckSound(name, sounds2D);
        if (s == null) return 0f;

        s.source.Play();
        return s.clip.length;
    }

    public void Stop2D(string name)
    {
        Sound s = CheckSound(name, sounds2D);
        if (s == null) return;

        s.source.Stop();
    }

    public void PlayAtPoint(string name, Vector3 position)
    {
        Sound s = CheckSound(name, sounds3D);
        if (s == null) return;

        AudioSource.PlayClipAtPoint(s.clip, position);
    }
}
