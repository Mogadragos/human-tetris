using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AudioTrigger : MonoBehaviour
{
    public string soundName;
    AudioManager manager;

    private void Awake()
    {
        manager = FindObjectOfType<AudioManager>();
    }

    void OnTriggerEnter(Collider collider)
    {
        manager.PlayAtPoint(soundName, transform.position);
    }
}

