using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class SoundCollision
{
    public string name;
    public float minVelocityMag;
}
