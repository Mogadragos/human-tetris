using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioSource))]
public class MenuAudioManager : MonoBehaviour
{
    public AudioSource hoverSource;
    public AudioSource clickSource;

    public void PlayClickSoundLeave()
    {
        AudioSource.PlayClipAtPoint(clickSource.clip, Camera.main.transform.position, 0.05f);
    }

    public void PlayClickSound()
    {
        clickSource.Play();
    }

    public void PlayHoverSound()
    {
        hoverSource.Play();
    }
}
