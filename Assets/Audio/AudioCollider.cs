using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AudioCollider : MonoBehaviour
{
    public SoundCollision[] sounds;
    IEnumerable<SoundCollision> orderedSounds;

    AudioManager manager;
    private void Awake()
    {
        manager = FindObjectOfType<AudioManager>();
        orderedSounds = sounds.OrderByDescending(s => s.minVelocityMag);
    }

    void OnCollisionEnter (Collision collision)
    {
        foreach(SoundCollision sound in orderedSounds)
        {
            if (collision.relativeVelocity.magnitude > sound.minVelocityMag)
            {
                manager.PlayAtPoint(sound.name, collision.contacts[0].point);
                break;
            }
        }
    }
}
