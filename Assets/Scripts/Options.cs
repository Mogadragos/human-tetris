using System.Collections;  
using System.Collections.Generic;  
using UnityEngine;  
using UnityEngine.SceneManagement;  



public class Options: MonoBehaviour {  

    public enum Rotation_Control
    {
        KeyBoard,
        Theremin,
        EyeTracking,
    }

    public enum Acceleration_Control
    {
        KeyBoard,
        Theremin,
        Mouse
    }

    public static int rotation_control = (int)Rotation_Control.KeyBoard;
    public static int deplacement_control = (int)Acceleration_Control.KeyBoard;

    public void PlayGame() {  
        SceneManager.LoadScene("Hub");  
    }  

    public void QuitGame() {  
        Application.Quit(); 
    }    

    public static void SetRotation(int value)
    {
        rotation_control = value;
        Debug.Log("Rotation = " + rotation_control + ", Deplacement = " + deplacement_control);
    }

    public static void SetDeplacement(int value)
    {
        deplacement_control = value;
        Debug.Log("Rotation = " + rotation_control + ", Deplacement = " + deplacement_control);
    }
}   