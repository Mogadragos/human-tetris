using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Security.Cryptography;

public class PointAnim : MonoBehaviour
{
    private bool shrinkable = true, dying;
    private float CurrentSize = 1;
    private void Awake()
    {
        transform.localScale = Vector3.zero;
        Appear();
    }

    public void Appear()
    {
        transform.DOScale(1, .15f).SetEase(Ease.InOutBack);
    }

    public void Shrink(float quantity)
    {
        if (dying == true) return;
        CurrentSize = Mathf.Clamp01(CurrentSize - (1 / quantity));
        //print(CurrentSize);
        if (shrinkable) transform.DOScale(CurrentSize, .15f).SetEase(Ease.OutQuint);
    }

    public void Die()
    {
        if (dying == true) return;
        dying = true;
        shrinkable = false;
        Sequence S = DOTween.Sequence();
        S.Append(transform.DOScale(0, .15f).SetEase(Ease.OutQuint));
        S.AppendCallback(() => Destroy(gameObject));
    }
}
