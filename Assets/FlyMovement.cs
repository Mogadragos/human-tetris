using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyMovement : MonoBehaviour
{
    public float InputRot, InputSpeed, Speed, RotSpeed;
    public Rigidbody RB;

    private void Update()
    {
        RB.velocity = Vector3.up * InputSpeed;
        RB.rotation = Quaternion.Euler(Vector3.forward * InputRot);
    }
}
