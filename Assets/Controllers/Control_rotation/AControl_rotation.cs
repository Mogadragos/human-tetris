using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AControl_rotation : MonoBehaviour
{
    public Transform fly_prefab;
    public GameObject Pivot;
    public Transform Mouth;
    public float rotation_speed;

    public AControl_rotation(Transform fly_prefab, GameObject pivot, Transform mouth, float rotation_speed)
    {
        this.fly_prefab = fly_prefab;
        Pivot = pivot;
        Mouth = mouth;
        this.rotation_speed = rotation_speed;
    }

    public abstract void Rotation();
   
}
