using UnityEngine;
using Tobii.Gaming;

public class Control_rotation_eyeTracker : AControl_rotation
{
    Camera cam;
    public Control_rotation_eyeTracker(Transform fly_prefab, GameObject pivot, Transform mouth, float rotation_speed, Camera cam) : base(fly_prefab, pivot, mouth, rotation_speed)
    {
        this.cam = cam;
    }

    public override void Rotation()
    {
        GazePoint gazePoint = TobiiAPI.GetGazePoint();
        if (gazePoint.IsValid)
        {
            Vector2 gazeRelativePosition = gazePoint.Viewport - (Vector2)cam.WorldToViewportPoint(fly_prefab.position);
            Vector2 direction = gazeRelativePosition.normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
            Quaternion quat = Quaternion.Euler(Vector3.forward * angle);
            fly_prefab.rotation = quat;

            Quaternion oppositeQuat = Quaternion.Inverse(quat);
            Pivot.transform.localRotation = oppositeQuat;

            Mouth.transform.rotation = Quaternion.Euler(angle, -90, -90);
        }
    }
}
