using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_rotation_MIDI : AControl_rotation
{
    public Control_rotation_MIDI(Transform fly_prefab, GameObject pivot, Transform mouth, float rotation_speed) : base(fly_prefab, pivot, mouth, rotation_speed)
    {
    }
    private void Start()
    {
        EasyController.esconsend.Send_data(1, 0);
        EasyController.esconsend.Send_data(2, 0);
    }

    public override void Rotation()
    {
        float rotation = EasyController.escon.get_state(2, -280f, 180f); // A regler
        rotation = Mathf.Clamp(rotation, -180f, 180f);

        fly_prefab.rotation = Quaternion.Euler(0, 0, rotation);
        Pivot.transform.rotation = Quaternion.Euler(0, 0, -fly_prefab.rotation.z);
        Mouth.transform.rotation = Quaternion.Euler(rotation, -90, -90);
    }
}
