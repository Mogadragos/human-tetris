using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_rotation_keyboard : AControl_rotation
{
    public Control_rotation_keyboard(Transform fly_prefab, GameObject pivot, Transform mouth, float rotation_speed) : base(fly_prefab, pivot, mouth, rotation_speed)
    {
       
        Mouth.transform.rotation = Quaternion.Euler(360, 90, -90);
        fly_prefab.transform.rotation = Quaternion.identity;
        Pivot.transform.rotation = Quaternion.identity;
    }

    public override void Rotation()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Pivot.transform.Rotate(0, 0, -rotation_speed);

            fly_prefab.transform.Rotate(0, 0, rotation_speed);

            Mouth.transform.Rotate(0, -rotation_speed, 0);

        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Pivot.transform.Rotate(0, 0, rotation_speed);

            fly_prefab.transform.Rotate(0, 0, -rotation_speed);
            Mouth.transform.Rotate(0, rotation_speed, 0);
        }
    }
}
