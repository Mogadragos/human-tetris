using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_focus : MonoBehaviour
{
    public Camera MainCamera;

    public float camera_player_euler_angle_difference;

    void Update()
    {
        camera_player_euler_angle_difference = Entities.player.transform.rotation.eulerAngles.z - MainCamera.transform.rotation.eulerAngles.z;

        //MainCamera.transform.Rotate(0, 0, camera_player_euler_angle_difference/4);

        //MainCamera.transform.eulerAngles += new Vector3(0, 0, camera_player_euler_angle_difference);
    }
}
