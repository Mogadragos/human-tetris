using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Control : MonoBehaviour
{
    [HideInInspector] public Rigidbody myRigidBody;

    public GameObject Pivot;

    public Transform Mouth;
    [HideInInspector]
    public AControl_rotation control_Rotation;
    [HideInInspector]
    public AControl_acceleration control_Acceleration;

    public Camera mainCamera;

    public float rotation_speed = 0.3f;

    public float velocity = 25;

    public float maxVelocity = 25, Frottement = 5f;

    public bool isInJelly = false;

    public bool canMove = true;

    public float eulerAngle;

    public float actual_velocity;

    public float AverageAngle;

    private static float sound1Cooldown = 0;
    private static float sound2Cooldown = 0;
    private static float sound3Cooldown = 0;
    private static float sound4Cooldown = 0;

    private bool isPaused = false;
    private Vector3 VelocityBeforePause = Vector3.zero;

    //private Queue<float> velocitiesMagnitudesQueue = new Queue<float>();
    private Queue<Vector3> velocitiesVectorsQueue = new Queue<Vector3>();

    protected virtual void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();

        InstantiateControl();
    }

    public void PausePlayer(bool pause)
    {
        if (pause)
        {
            VelocityBeforePause = myRigidBody.velocity;
            myRigidBody.velocity = Vector3.zero;
            isPaused = pause;
        }
        else
        {
            myRigidBody.velocity = VelocityBeforePause;
            isPaused = pause;
        }
    }

    public void InstantiateControl()
    {
        switch (Options.rotation_control)
        {
            case (int)Options.Rotation_Control.KeyBoard:
                control_Rotation = new Control_rotation_keyboard(transform, Pivot, Mouth, rotation_speed);
                break;
            case (int)Options.Rotation_Control.Theremin:
                control_Rotation = new Control_rotation_MIDI(transform, Pivot, Mouth, rotation_speed);
                break;
            case (int)Options.Rotation_Control.EyeTracking:
                control_Rotation = new Control_rotation_eyeTracker(transform, Pivot, Mouth, rotation_speed, mainCamera);
                break;
        }

        switch (Options.deplacement_control)
        {
            case (int)Options.Acceleration_Control.KeyBoard:
                control_Acceleration = new Control_acceleration_keyboard(this, velocity);
                break;
            case (int)Options.Acceleration_Control.Theremin:
                control_Acceleration = new Control_acceleration_MIDI(this, velocity);
                break;
            case (int)Options.Acceleration_Control.Mouse:
                control_Acceleration = new Control_acceleration_Mouse(this, velocity);
                break;
        }
    }

    public Vector2 GetThrust()
    {
        eulerAngle = transform.rotation.eulerAngles.z + 90;
        return new Vector2(Mathf.Cos(eulerAngle * Mathf.Deg2Rad), Mathf.Sin(eulerAngle * Mathf.Deg2Rad));

    }

    void Update()
    {
        if (isPaused)
            return;

        if (canMove)
        {
            control_Rotation.Rotation();
        }
        control_Acceleration.Acceleration();

        VelocitySound();

        actual_velocity = myRigidBody.velocity.magnitude;


        //float MaxSpeed = Mathf.Min(maxVelocity, myRigidBody.velocity.magnitude - Frottement);

        if (isInJelly)
            myRigidBody.velocity = Vector3.ClampMagnitude(myRigidBody.velocity, Mathf.Max(0, 6f));

        Deceleration();
    }

    private void VelocitySound()
    {
        velocitiesVectorsQueue.Enqueue(myRigidBody.velocity);
        //velocitiesMagnitudesQueue.Enqueue(myRigidBody.velocity.magnitude);

        if (velocitiesVectorsQueue.Count < 10) return;
        velocitiesVectorsQueue.Dequeue();
        //velocitiesMagnitudesQueue.Dequeue();

        sound1Cooldown--;
        sound2Cooldown--;
        sound3Cooldown--;
        sound4Cooldown--;

        if (myRigidBody.velocity.magnitude < maxVelocity / 7) return;
        //if (framesSinceLastSound < soundCooldown) return;

            //float velocityAverage = velocitiesMagnitudesQueue.Average();
            Vector3 average = Vector3.zero;
        foreach (Vector3 vector in velocitiesVectorsQueue)
        {
            average += vector;
        }

        float angleAverageDifference = Vector3.Angle(average, GetThrust());
        AverageAngle = angleAverageDifference;

        //float velocityDifference = myRigidBody.velocity.magnitude  - velocityAverage;

        if (angleAverageDifference > 90)
        {
            if (myRigidBody.velocity.magnitude > maxVelocity / 5)
            {
                if (sound1Cooldown > 0) return;

                AudioManager.instance.Play2D("RotationFastStrong");

                sound1Cooldown = 200;
            }
            else
            {
                if (sound2Cooldown > 0) return;
                AudioManager.instance.Play2D("RotationSlowStrong");

                sound2Cooldown = 200;
            }
        }
        else if (angleAverageDifference > 45)
        {
            if (myRigidBody.velocity.magnitude > maxVelocity / 5)
            {
                if (sound3Cooldown > 0) return;
                AudioManager.instance.Play2D("RotationFastMedium");

                sound3Cooldown = 200;
            }
            else
            {
                if (sound4Cooldown > 0) return;
                AudioManager.instance.Play2D("RotationSlowMedium");

                sound4Cooldown = 200;
            }
        }
    }

    public void Deceleration()
    {
        float MaxSpeed = Mathf.Min(maxVelocity, myRigidBody.velocity.magnitude - Frottement);
        myRigidBody.velocity = Vector3.ClampMagnitude(myRigidBody.velocity,Mathf.Max(0, MaxSpeed));
    }
}

