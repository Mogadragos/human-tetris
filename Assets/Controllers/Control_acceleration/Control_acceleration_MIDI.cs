using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_acceleration_MIDI : AControl_acceleration
{
    public Control_acceleration_MIDI(Control parent, float velocity) : base(parent, velocity)
    {
    }

    private void Start()
    {
        EasyController.esconsend.Send_data(1, 0);
        EasyController.esconsend.Send_data(2, 0);
    }

    public override void Acceleration()
    {
        float boost = EasyController.escon.get_state(1, 1f, 0.0f);
        boost = Mathf.Clamp(boost - 0.16f, 0, 10f);

        thrust = parent.GetThrust();

        myRigidBody.AddForce(new Vector2(boost * velocity * thrust.x, boost * velocity * thrust.y));
    }
}
