using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_acceleration_Mouse : AControl_acceleration
{
    public Control_acceleration_Mouse(Control parent, float velocity) : base(parent, velocity)
    {
    }

    public override void Acceleration()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            thrust = parent.GetThrust();
            myRigidBody.AddForce(new Vector2(velocity * thrust.x, velocity * thrust.y));
        }
    }
}
