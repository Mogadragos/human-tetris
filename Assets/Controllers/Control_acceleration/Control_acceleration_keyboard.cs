using UnityEngine;

public class Control_acceleration_keyboard : AControl_acceleration
{
    public Control_acceleration_keyboard(Control parent, float velocity) : base(parent, velocity)
    {
    }

    public override void Acceleration()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            thrust = parent.GetThrust();
            myRigidBody.AddForce(new Vector2(velocity* thrust.x, velocity* thrust.y));
        }
    }
}
