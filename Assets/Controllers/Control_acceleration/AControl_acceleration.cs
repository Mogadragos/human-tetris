using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AControl_acceleration : MonoBehaviour
{
    public Vector2 thrust;
    public Control parent;
    public float velocity;
    public Rigidbody myRigidBody;

    protected AControl_acceleration(Control parent, float velocity)
    {
        this.parent = parent;
        this.velocity = velocity;

        myRigidBody = parent.GetComponent<Rigidbody>();
    }

    public abstract void Acceleration();
}
